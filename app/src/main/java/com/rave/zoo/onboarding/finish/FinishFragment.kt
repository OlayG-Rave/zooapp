package com.rave.zoo.onboarding.finish

import android.content.Intent
import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.rave.zoo.home.ZooActivity
import com.rave.zoo.databinding.FragmentFinishBinding
import com.rave.zoo.onboarding.ZooOnBoardingActivity

class FinishFragment : Fragment() {

    private var _binding: FragmentFinishBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentFinishBinding.inflate(
        inflater, container, false
    ).also { _binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnBack.setOnClickListener { findNavController().navigateUp() }
        binding.btnFinish.setOnClickListener {
            val intent = Intent(activity as ZooOnBoardingActivity, ZooActivity::class.java)
            intent.putExtra(ZooActivity.EXTRA_ZOO_NAME, "Zaboomazoo")
            startActivity(intent)
            activity?.finish()
        }
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}