package com.rave.zoo.home.animals

import android.os.Bundle
import android.view.LayoutInflater
import android.view.View
import android.view.ViewGroup
import androidx.fragment.app.Fragment
import androidx.navigation.fragment.findNavController
import com.rave.zoo.databinding.FragmentHomeBinding

class HomeFragment : Fragment() {

    private var _binding: FragmentHomeBinding? = null
    private val binding get() = _binding!!

    override fun onCreateView(
        inflater: LayoutInflater,
        container: ViewGroup?,
        savedInstanceState: Bundle?
    ) = FragmentHomeBinding.inflate(
        inflater, container, false
    ).also { _binding = it }.root

    override fun onViewCreated(view: View, savedInstanceState: Bundle?) {
        super.onViewCreated(view, savedInstanceState)
        binding.btnLion.setOnClickListener {
            navigateToDetails(name = "Lion", fact = "Males are lazy")
        }
        binding.btnTiger.setOnClickListener {}
        binding.btnBear.setOnClickListener {}
        binding.btnBird.setOnClickListener {}
    }

    private fun navigateToDetails(name: String, fact: String) {
        val detailDirections =
            HomeFragmentDirections.goToDetailFragment(name, fact)
        findNavController().navigate(detailDirections)
    }

    override fun onDestroyView() {
        super.onDestroyView()
        _binding = null
    }

}