package com.rave.zoo.home

import android.os.Bundle
import android.widget.Toast
import androidx.appcompat.app.AppCompatActivity
import com.rave.zoo.R

class ZooActivity : AppCompatActivity(R.layout.activity_zoo) {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        val zooName = intent.extras?.get(EXTRA_ZOO_NAME)?.toString()
        Toast.makeText(this, "Welcome to $zooName", Toast.LENGTH_SHORT).show()
    }

    companion object {
        const val EXTRA_ZOO_NAME = "extra_zoo_name"
    }
}